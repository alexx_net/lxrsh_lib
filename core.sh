#
# the core bash library
#

# This should deal with variable lib locations
# downloading lib files from any URI (file://var/lib ~/.lib/sh/ https://raw.github)
# and deal with library versions.

#export SH_LIB=~/lib/sh # FAIL
#export SH_LIB="~/lib/sh" # FAIL
export SH_LIB="${HOME}/.local/lxrsh_lib" # OK
#export SH_LIB=$'~/lib/sh' # FAIL
#export SH_LIB=$'~/lib/sh' # FAIL

#####################
## function_exists ##
#####################

function_exists() {
    declare -f -F $1 > /dev/null
    return $?
}

###############
## fn_exists ##
###############

fn_exists(){
    type -t "$1" | grep -q 'shell function'
}

#####################
## core_is_loaded ###
#####################
# Usage: 
#
# if [ "$SH_LIB" ]; then
#   type core_is_loaded &>/dev/null || . $SH_LIB/core.sh
# else
#   . ~/.local/lib/sh/core.sh;
# fi
#
core_is_loaded() {
    return 1
}
